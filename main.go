package main

import (
	"net/http"
	"os"
	"strings"
	"unicode"

	"github.com/bwmarrin/discordgo"
	"github.com/go-kit/kit/log"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Variables used for command line parameters
var (
	requests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "lfg_bot_requests_total",
			Help: "Number of lfg requests processed by channel",
		},
		[]string{"channel"},
	)

	errors = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "lfg_bot_errors_total",
		Help: "Total number of errors from improperly formatted commands",
	})

	messages_seen = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "lfg_messages_seen_total",
		Help: "Total number of messages seen by the bot",
	})
)

func init() {
	prometheus.MustRegister(requests)
	prometheus.MustRegister(errors)
	prometheus.MustRegister(messages_seen)
}

func main() {
	logger := log.NewLogfmtLogger(os.Stdout)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)

	token, ok := os.LookupEnv("DISCORD_BOT_TOKEN")
	if !ok {
		logger.Log("msg", "DISCORD_BOT_TOKEN can not be empty")
		os.Exit(1)
	}

	dg, err := discordgo.New("Bot " + token)
	defer dg.Close()
	if err != nil {
		logger.Log("msg", "error creating Discord session", "error", err)
	}

	dg.AddHandler(messageCreate)

	err = dg.Open()
	if err != nil {
		logger.Log("msg", "error opening connection", "error", err)
		return
	}

	http.Handle("/metrics", promhttp.Handler())
	logger.Log("event", "starting Prometheus server on :4321")
	logger.Log("error", http.ListenAndServe(":4321", nil))
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	logger := log.NewLogfmtLogger(os.Stdout)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)

	// Ignore all messages created by the bot itself
	if m.Author.ID == s.State.User.ID && m.Author.Bot {
		return
	}

	// Lets only track non-bot messages
	messages_seen.Inc()

	if !strings.HasPrefix(m.Content, "!lfg ") {
		return
	}

	if strings.EqualFold(m.Content, "!lfg usage") {
		_, err := s.ChannelMessageSend(m.ChannelID, "Call me with: `!lfg #lfg_from_picker <your request>`. If you're unsure, ask a Moderator!")
		if err != nil {
			errors.Inc()
			logger.Log("msg", err)
			return
		}
		return
	}

	if len(strings.Fields(m.Content)) < 3 {
		pcid, err := s.UserChannelCreate(m.Author.ID)
		if err != nil {
			errors.Inc()
			logger.Log("msg", err)
		}

		_, err = s.ChannelMessageSend(pcid.ID, "Hi "+m.Author.Mention()+"! Call me with: `!lfg #lfg_from_picker <your request>`. If you're unsure, ask a Moderator!")
		if err != nil {
			errors.Inc()
			logger.Log("msg", err, "user", m.Author.Mention())
		}

		errors.Inc()
		logger.Log("msg", "expected at least 3 fields", "user", m.Author.Mention())
		return
	}

	cmd := strings.Fields(m.Content)

	// Strip anything that isn't a number/letter
	// Discord IDs are uint64 represented as a string
	cid := strings.TrimFunc(cmd[1], func(r rune) bool {
		return !unicode.IsNumber(r)
	})

	// Get the channel
	cn, err := s.Channel(cid)
	if err != nil {
		errors.Inc()
		logger.Log("msg", "channel not found", "error", err)
		return
	}

	if !strings.Contains(cn.Name, "lfg") {
		pcid, err := s.UserChannelCreate(m.Author.ID)
		if err != nil {
			errors.Inc()
			logger.Log("msg", err)
		}

		_, err = s.ChannelMessageSend(pcid.ID, "Hi "+m.Author.Mention()+"! Choose a channel that contains \"lfg\" within the name. Thank you!")
		if err != nil {
			errors.Inc()
			logger.Log("msg", err, "user", m.Author.Mention())
		}
		return
	}

	// Get the channel's parent "channel" (this is really the category, but shares the channel struct)
	pcn, err := s.Channel(cn.ParentID)
	if err != nil {
		errors.Inc()
		logger.Log("error", err)
		return
	}

	_, err = s.ChannelMessageSend(cid, m.Author.Mention()+" is looking for "+strings.Join(cmd[2:], " "))
	if err != nil {
		errors.Inc()
		logger.Log("error", err)
		return
	}

	err = s.ChannelMessageDelete(m.ChannelID, m.ID)
	if err != nil {
		errors.Inc()
		logger.Log("msg", "can not delete message", "err", err)
	}

	pc := strings.TrimFunc(pcn.Name, func(r rune) bool {
		return !unicode.IsLetter(r) && !unicode.IsDigit(r)
	})
	c := strings.Replace(strings.ToLower(pc), " ", "_", -1) + "_" + strings.Replace(strings.ToLower(cn.Name), " ", "_", -1)

	requests.With(prometheus.Labels{"channel": c}).Inc()
}
