# lfg-bot

A really basic bot that listens for `!lfg` commands to direct a LFG style message to a channel.

Usage: `!lfg #d2_lfg_pc need 2 for EP farm`

## Building

`go build`, the best way.

## Installing and running

systemd

```
sudo cp ./systemd/lfg-bot.service /etc/systemd/service/lfg-bot.service 
sudo systemctl daemon-reload
sudo systemctl enable lfg-bot
sudo systemctl start lfg-bot
sudo systemctl status lfg-bot
```

docker

```
coming soon
```